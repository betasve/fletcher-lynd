ActiveAdmin.register University do
	actions :all

	menu priority: 6

	index do 
		column :id
		column :name, sortable: :name do |university|
			link_to university.name, admin_university_path(university)
		end
		column :created_at, sortable: :created_at do |university|
			university.created_at.strftime("%d %h %Y %H:%M")
		end
		actions
	end

	form do |f|
	    f.inputs do
	      f.input :name
	    end
    	f.actions
  	end

	permit_params :name
end