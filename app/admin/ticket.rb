ActiveAdmin.register Ticket do
	actions :all, :except => [:destroy, :new, :edit]

	menu priority: 2

	index do 
		column :first_name, sortable: :first_name
		column :last_name, sortable: :last_name
		column :email, sortable: :email
		column :created_at, sortable: :created_at do |ticket|
			ticket.created_at.strftime("%d %h %Y %H:%M")
		end
		actions
	end

	show do |ticket|
		attributes_table do
			row :id
			row :first_name
			row :last_name
			row :birthday
			row :passport_num
			row :passport_valid_date
			row :mobile
			row :email
			row :destination
			row :departure_date
			row :comeback_date
			row :airlines
			row :arrival_time
			row :comments
			row :created_at
		end
	end

	permit_params :first_name, :last_name, :fathers_name, :birthday, :passport_num, :passport_valid_date, :mobile, :email, :destination, :departure_date, :comeback_date, :airlines, :arrival_time, :comments
end