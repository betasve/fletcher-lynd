ActiveAdmin.register Settings do

  actions :all, :except => [:destroy]

  index do
    column :setting_name, sortable: :setting_name do |setting|
      link_to setting.setting_name, admin_setting_path(setting)
    end
    column :created_at, sortable: :created_at do |setting|
      setting.created_at.strftime("%d %h %Y %H:%M")
    end
    column :updated_at, sortable: :created_at do |setting|
      setting.updated_at.strftime("%d %h %Y %H:%M")
    end
    actions
  end

  form do |f|
    f.inputs do
      if f.object.setting_name.present?
        f.input :setting_name, as: :select, collection: [f.object.setting_name] + Settings.get_available_settings
      else
        f.input :setting_name, as: :select, collection: Settings.get_available_settings
      end
      f.input :setting_en
      f.input :setting_bg
    end
    f.actions
  end

  show do |form|
    attributes_table do
      row :setting_name
      row :setting_en
      row :setting_bg
      row :created_at
      row :updated_at
    end
  end

  permit_params :setting_name, :setting_en, :setting_bg
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
