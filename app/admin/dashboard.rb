ActiveAdmin.register_page "Dashboard" do

  menu priority: 0, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    #div class: "blank_slate_container", id: "dashboard_default_message" do
    #  span class: "blank_slate" do
    #    span I18n.t("active_admin.dashboard_welcome.welcome")
    #    small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #  end
    #end


    columns do
      column do
        panel "Applications" do
          table_for Form.last(5).map do |form|
            column("Name") { |form| "#{form.first_name} #{form.last_name}"}
            column("Date") { |form| form.created_at.strftime("%d %h %Y %H:%M") }
            column("Actions") { |form| link_to 'View', admin_form_path(form) }
          end
        end
      end

      column do
        panel "Tickets" do
          table_for Ticket.last(5).map do |form|
            column("Name") { |form| "#{form.first_name} #{form.last_name}"}
            column("Date") { |form| form.created_at.strftime("%d %h %Y %H:%M") }
            column("Actions") { |form| link_to 'View', admin_ticket_path(form) }
          end
        end
      end
    end

    columns do
      column do
        panel "Offers" do
          table_for Offer.last(5).map do |offer|
            column("Title") { |offer| "#{offer.title}"}
            column("Date") { |offer| offer.created_at.strftime("%d %h %Y %H:%M") }
            column("Actions") do |offer| 
              text_node link_to 'View', admin_offer_path(offer)
              text_node '  '
              text_node link_to 'Edit', edit_admin_offer_path(offer)
              text_node '  '
              text_node link_to 'Delete', admin_offer_path(offer), method: :delete, data: { confirm: 'Are you sure?'}
            end
          end
        end
      end

      column do
        panel "Static Pages" do
          table_for StaticPage.last(5).map do |page|
            column("Name") { |page| "#{page.title}"}
            column("Date") { |page| page.created_at.strftime("%d %h %Y %H:%M") }
            column("Actions") do |page|
              text_node link_to 'View', admin_static_page_path(page)
              text_node '  '
              text_node link_to 'Edit', edit_admin_static_page_path(page)
              text_node '  '
              text_node link_to 'Delete', admin_static_page_path(page), method: :delete, data: { confirm: 'Are you sure?'}
            end
          end
        end
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
