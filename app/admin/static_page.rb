ActiveAdmin.register StaticPage do

  #actions :all, :except => [:destroy]

  menu priority: 4

  filter :in_main_menu

  index do
    column :in_main_menu
    column :in_slider
    column :title_en, sortable: :title_en do |page|
      link_to page.title_en, admin_static_page_path(page)
    end
    column :slug
    column :page_order
    column :created_at, sortable: :created_at do |page|
      page.created_at.strftime("%d %h %Y %H:%M")
    end
    column :updated_at, sortable: :updated_at do |page|
      page.updated_at.strftime("%d %h %Y %H:%M")
    end
    actions
  end

  form do |f|
    f.inputs do
      f.input :title_en
      f.input :title_bg
      f.input :in_main_menu, label: "Page is visible in the site's main menu"
      f.input :in_slider, label: "Page's banner is visible in the site's slider"
      f.input :page_order
      if f.object.banner.present?
        f.input :banner, as: :file, hint: f.template.image_tag(f.object.banner.url(:thumb))
      else
        f.input :banner, as: :file, hint: "Only files with extension .jpg, .jpeg, .gif and .png are allowed."
      end
      f.input :banner_caption_en
      f.input :banner_caption_bg
      f.input :banner_description_en
      f.input :banner_description_bg
      f.input :body_en, :input_html => { :class => "tinymce", :id => "body_en" }
      f.input :body_bg, :input_html => { :class => "tinymce", :id => "body_bg" }
      f.input :slug
    end
    f.actions
  end

  show do |form|
    attributes_table do
      row :id
      row :title_en
      row :title_bg
      row :in_main_menu, label: "Visible in main menu"
      row :in_slider, label: "Visible in slider"
      row :page_order
      row :banner do
        image_tag(form.banner.thumb) if form.banner.present?
      end
      row :banner_caption_en
      row :banner_caption_bg
      row :banner_description_en
      row :banner_description_bg
      row :body_bg do
        simple_format form.body_bg.html_safe
      end
      row :body_en do
        form.body_en.html_safe
      end
      row :slug
      row :created_at
      row :updated_at
    end
  end

  permit_params :title_en, :title_bg, :body_en, :body_bg, :in_main_menu, :banner, :banner_caption_bg, :banner_caption_en, :banner_description_bg, :banner_description_en, :slug, :in_slider, :page_order
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
