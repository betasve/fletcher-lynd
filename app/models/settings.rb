class Settings < ActiveRecord::Base

	validates :setting_name, presence: true, uniqueness: true
	validates :setting_bg, presence: true
	validates :setting_en, presence: true

	translates :setting

	SETTINGS = [
			'Application form description', 
			'Ticket form description', 
			'Contact form description',
			'Jobs Fletcher Lynd link',
			'Online job fair',
			'Prices and promotions link',
			'HP yellow box text',
			'HP purple box text',
			'HP first white box text',
			'HP second white box text',
			'HP after online job fair box text',
			'HP slider contact information'
		]

	def self.get_available_settings
		already_set = Settings.all.map(&:setting_name)
		SETTINGS - already_set
	end
end
