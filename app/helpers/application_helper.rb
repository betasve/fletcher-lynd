module ApplicationHelper
	def generate_prices_and_terms_link
		@prices_and_temrs = Settings.where(setting_name: 'Prices and promotions link').first
		if @prices_and_temrs.present?
			locale = params[:locale] || 'bg'
			slug = @prices_and_temrs.setting.gsub('<p>', '').gsub('</p>', '')
			"#{locale}/#{slug}"
		end
	end
end
