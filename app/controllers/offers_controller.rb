class OffersController < ApplicationController
	before_action :set_locale
	def show
		@page = Offer.friendly.find(params[:friendly_id])
	end
end