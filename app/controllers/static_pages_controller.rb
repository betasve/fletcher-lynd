class StaticPagesController < ApplicationController
	before_action :set_locale
	def show
		@page = StaticPage.friendly.find(params[:friendly_id])
	end
end