class HomeController < ApplicationController
  before_action :set_locale

  helper_method :generate_job_fair_link

  def index
    @static_pages_headers = StaticPage.where(in_slider: true)
    @yellow_box_text = Settings.where(setting_name: 'HP yellow box text').first
    @purple_box_text = Settings.where(setting_name: 'HP purple box text').first
    @jobs_fletcher_lynd_link = Settings.where(setting_name: 'Jobs Fletcher Lynd link').first
    @first_white_box = Settings.where(setting_name: 'HP first white box text').first
    @second_white_box = Settings.where(setting_name: 'HP second white box text').first
    @online_job_fair = Settings.where(setting_name: 'Online job fair').first
    @after_job_fair_text = Settings.where(setting_name: 'HP after online job fair box text').first
    @contacts_slider = Settings.where(setting_name: 'HP slider contact information').first
    @offers = Offer.where(visible: true).order(offer_order: :asc)
  end

  def generate_job_fair_link
    if @online_job_fair.present?
      locale = params[:locale] || 'bg'
      slug = @online_job_fair.setting.gsub('<p>', '').gsub('</p>', '')
      "#{locale}/#{slug}"
    end
  end
end
