
(function() {
    function stuffForResizeAndReady(){
       // What happens on ready and resize state
       // Creating break point of when Menu Button is goin to show or hide
        if ($(window).width() < 1006) {
            $("#menu").css("display", "none");
        } else {
            $("#menu").css("display", "block");
        }
    }

    $(window).on("resize", stuffForResizeAndReady);
    $(document).on("ready", stuffForResizeAndReady);
}());

// Toggle Menu on menuBtn click
$(document).on("ready", function(){
    $("#menuButton").on("click",function(){
        $("#menu").slideToggle();
    });
});