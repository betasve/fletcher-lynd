class AddBannerToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :banner, :string
  end
end
