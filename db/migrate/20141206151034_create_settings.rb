class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :setting_name
      t.text :setting_bg
      t.text :setting_en

      t.timestamps
    end
  end
end
