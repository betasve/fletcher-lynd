class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :title_bg
      t.string :title_en
      t.text :description_bg
      t.text :description_en
      t.text :locations_bg
      t.text :locations_en
      t.text :body_bg
      t.text :body_en
      t.boolean :visible

      t.timestamps
    end
  end
end
