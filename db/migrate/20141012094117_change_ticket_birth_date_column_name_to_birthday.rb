class ChangeTicketBirthDateColumnNameToBirthday < ActiveRecord::Migration
  def self.up
  	rename_column :tickets, :birth_date, :birthday
  end

  def self.down
  	rename_column :tickets, :birthday, :birth_date
  end
end
