class CreateStaticPages < ActiveRecord::Migration
  def change
    create_table :static_pages do |t|
      t.string :title_en
      t.string :title_bg
      t.text :body_en
      t.text :body_bg

      t.timestamps
    end
  end
end
