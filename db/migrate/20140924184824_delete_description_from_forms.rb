class DeleteDescriptionFromForms < ActiveRecord::Migration
  def self.up
  	remove_column :forms, :description_bg
  	remove_column :forms, :description_en
  end

  def self.down
  	add_column :forms, :description_bg, :text
  	add_column :forms, :description_en, :text
  end
end
