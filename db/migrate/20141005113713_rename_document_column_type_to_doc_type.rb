class RenameDocumentColumnTypeToDocType < ActiveRecord::Migration
  def self.up
  	rename_column :documents, :type, :doc_type
  end

  def self.down
  	rename_column :documents, :doc_type, :type
  end
end
