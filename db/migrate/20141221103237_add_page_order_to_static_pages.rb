class AddPageOrderToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :page_order, :integer, default: 0
  end
end
