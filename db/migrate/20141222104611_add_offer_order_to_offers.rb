class AddOfferOrderToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :offer_order, :integer, default: 0
  end
end
