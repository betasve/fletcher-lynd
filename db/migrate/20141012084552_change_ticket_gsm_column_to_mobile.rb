class ChangeTicketGsmColumnToMobile < ActiveRecord::Migration
  def self.up
  	rename_column :tickets, :gsm, :mobile
  end

  def self.down
  	rename_column :tickets, :mobile, :gsm
  end
end
