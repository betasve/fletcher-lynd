class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.string :location
      t.string :type
      t.integer :static_page_id

      t.timestamps
    end
  end
end
