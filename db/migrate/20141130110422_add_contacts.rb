class AddContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :names
      t.string :email
      t.text :message

      t.timestamps
    end
  end
end