class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.text :description_en
      t.text :description_bg
      t.string :last_name
      t.string :first_name
      t.string :fathers_name
      t.datetime :birthday
      t.string :place_of_birth
      t.string :mobile
      t.string :skype
      t.string :university
      t.string :year_of_study
      t.string :email
      t.string :fletcher_office

      t.timestamps
    end
  end
end
